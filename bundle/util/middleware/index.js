"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hashpwd = exports.decodeToken = exports.Authen = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _bcrypt = _interopRequireDefault(require("bcrypt"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

require('dotenv').config();

var key = process.env.PRIVATE_KEY;

var Authen = function Authen(req, res, next) {
  var token = req.headers['authorization'];

  if (token === undefined) {
    res.json({
      code: 0,
      msg: 'Chưa đăng nhập'
    });
  } else {
    _jsonwebtoken["default"].verify(token, key, {
      algorithm: 'HS512'
    }, function (err) {
      if (err) res.json({
        code: 0,
        msg: 'Token error'
      });else next();
    });
  }
};

exports.Authen = Authen;

var decodeToken =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(_ref) {
    var authorization, token;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            authorization = _ref.headers.authorization;
            _context.next = 3;
            return _jsonwebtoken["default"].verify(authorization, key, {
              algorithm: 'HS512'
            });

          case 3:
            token = _context.sent;
            return _context.abrupt("return", token['_doc']);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function decodeToken(_x) {
    return _ref2.apply(this, arguments);
  };
}();

exports.decodeToken = decodeToken;

var hashpwd =
/*#__PURE__*/
function () {
  var _ref3 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(pwd) {
    var hash;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _bcrypt["default"].hashSync(pwd, 10);

          case 2:
            hash = _context2.sent;
            return _context2.abrupt("return", hash);

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function hashpwd(_x2) {
    return _ref3.apply(this, arguments);
  };
}();

exports.hashpwd = hashpwd;