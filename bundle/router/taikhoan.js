"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaiKhoan = void 0;

var _express = _interopRequireDefault(require("express"));

var _v = _interopRequireDefault(require("uuid/v4"));

var _util = require("../util");

var _schema = require("../schema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var TaiKhoan = _express["default"].Router();

exports.TaiKhoan = TaiKhoan;
TaiKhoan.get('/', _util.Authen,
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(req, res) {
    var _listTaiKhoan;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _schema.taiKhoan.find({});

          case 2:
            _listTaiKhoan = _context.sent;
            res.json(_toConsumableArray(_listTaiKhoan));

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
TaiKhoan.get('/:ID', _util.Authen,
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(req, res) {
    var ID, _taiKhoan;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            ID = req.params.ID;
            _context2.next = 3;
            return _schema.taiKhoan.findOne({
              _id: ID
            });

          case 3:
            _taiKhoan = _context2.sent;
            res.json(_taiKhoan);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
TaiKhoan.post('/',
/*#__PURE__*/
function () {
  var _ref3 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(req, res) {
    var data, token, info, logs, _taiKhoan;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            data = req.body;
            token = req.headers['authorization'];

            if (!(token !== undefined)) {
              _context3.next = 9;
              break;
            }

            _context3.next = 5;
            return (0, _util.decodeToken)(req);

          case 5:
            info = _context3.sent;

            if (info.code === 'admin') {
              data.code = 'mod';
            } else {
              data.code = 'member';
            }

            _context3.next = 10;
            break;

          case 9:
            data.code = 'member';

          case 10:
            _context3.next = 12;
            return _schema.taiKhoan.findOne({
              taiKhoan: data.taiKhoan
            });

          case 12:
            logs = _context3.sent;

            if (!(logs !== null)) {
              _context3.next = 17;
              break;
            }

            res.json({
              code: 0,
              msg: 'Tài khoản đã tồn tại'
            });
            _context3.next = 25;
            break;

          case 17:
            data._id = (0, _v["default"])();
            _context3.next = 20;
            return (0, _util.hashpwd)(data.matKhau);

          case 20:
            data.matKhau = _context3.sent;
            data.ngaySinh = +data.ngaySinh;
            _taiKhoan = new _schema.taiKhoan(data);

            _taiKhoan.save();

            res.json({
              code: 1
            });

          case 25:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}());
TaiKhoan.put('/:ID', _util.Authen,
/*#__PURE__*/
function () {
  var _ref4 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(req, res) {
    var ID, data, logs, _taiKhoan;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            ID = req.params.ID;
            data = req.body;
            _context4.next = 4;
            return _schema.taiKhoan.findOne({
              _id: ID
            });

          case 4:
            logs = _context4.sent;

            if (!(logs === null)) {
              _context4.next = 9;
              break;
            }

            res.json({
              code: 0,
              msg: 'Tài khoản không tồn tại'
            });
            _context4.next = 13;
            break;

          case 9:
            _context4.next = 11;
            return _schema.taiKhoan.findOneAndUpdate({
              _id: ID
            }, data, {
              "new": true
            });

          case 11:
            _taiKhoan = _context4.sent;
            res.json(_taiKhoan);

          case 13:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}());
TaiKhoan["delete"]('/:ID', _util.Authen,
/*#__PURE__*/
function () {
  var _ref5 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5(req, res) {
    var ID, logs;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            ID = req.params.ID;
            _context5.next = 3;
            return _schema.taiKhoan.findOne({
              _id: ID
            });

          case 3:
            logs = _context5.sent;

            if (!(logs === null)) {
              _context5.next = 8;
              break;
            }

            res.json({
              code: 0,
              msg: 'Tài khoản không tồn tại'
            });
            _context5.next = 11;
            break;

          case 8:
            _context5.next = 10;
            return _schema.taiKhoan.findOneAndRemove({
              _id: ID
            });

          case 10:
            res.json({
              code: 1
            });

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}());