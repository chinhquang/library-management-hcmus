"use strict";

require("@babel/polyfill");

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _router = require("./router");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

require('dotenv').config();

_asyncToGenerator(
/*#__PURE__*/
regeneratorRuntime.mark(function _callee() {
  var app, port;
  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          app = (0, _express["default"])();
          app.use((0, _cors["default"])());
          app.use(_bodyParser["default"].json());
          port = process.env.PORT || 12345;
          _context.next = 6;
          return _mongoose["default"].connect("".concat(process.env.DB_URL, "/").concat(process.env.DB_NAME), {
            useNewUrlParser: true,
            dbName: 'lib'
          }).then(function () {
            console.log("\uD83D\uDE80 Database connected.");
          })["catch"](function (e) {
            throw e;
          });

        case 6:
          app.use('/api', _router.router);
          app.listen(port, function () {
            console.log("\uD83D\uDE80 Server already running on http://localhost:".concat(port));
          });

        case 8:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}))();