"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.taiKhoan = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var taiKhoan_schema = new _mongoose["default"].Schema({
  _id: String,
  hoTen: String,
  ngaySinh: Number,
  taiKhoan: String,
  matKhau: String,
  SDT: String,
  truongDH: String,
  mssv: String,
  code: String
});

var taiKhoan = _mongoose["default"].model('taiKhoan', taiKhoan_schema);

exports.taiKhoan = taiKhoan;