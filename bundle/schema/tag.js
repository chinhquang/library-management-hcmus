"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tag = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var tag_schema = new _mongoose["default"].Schema({
  _id: String,
  name: String
});

var tag = _mongoose["default"].model('tag', tag_schema);

exports.tag = tag;