"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sach = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var sach_schema = new _mongoose["default"].Schema({
  _id: String,
  tenSach: String,
  tenTacGia: String,
  ngayXuatBan: Number,
  trangThai: Number,
  seri: String,
  tag: [{
    type: String,
    ref: 'tag'
  }]
});

var sach = _mongoose["default"].model('sach', sach_schema);

exports.sach = sach;