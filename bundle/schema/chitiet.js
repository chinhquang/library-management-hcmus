"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chiTiet = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var chiTiet_schema = new _mongoose["default"].Schema({
  _id: String,
  _sach: [{
    type: String,
    ref: 'sach'
  }],
  ghiChu: String,
  ngayMuon: Number,
  ngayHenTra: Number,
  ngayTra: Number,
  tongTien: Number,
  trangThai: Number
});

var chiTiet = _mongoose["default"].model('chiTiet', chiTiet_schema);

exports.chiTiet = chiTiet;