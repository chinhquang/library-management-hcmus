"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.muonSach = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var muonSach_schema = new _mongoose["default"].Schema({
  _id: String,
  _nguoiMuon: {
    type: String,
    ref: 'taiKhoan'
  },
  _nguoiChoMuon: {
    type: String,
    ref: 'taikhoan'
  },
  _chiTiet: {
    type: String,
    ref: 'chiTiet'
  }
});

var muonSach = _mongoose["default"].model('muonSach', muonSach_schema);

exports.muonSach = muonSach;