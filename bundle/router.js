"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.router = void 0;

var _express = _interopRequireDefault(require("express"));

var _taikhoan = require("./router/taikhoan");

var _sach = require("./router/sach");

var _login = require("./router/login");

var _tag = require("./router/tag");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

exports.router = router;
router.use('/taikhoan', _taikhoan.TaiKhoan);
router.use('/sach', _sach.Sach);
router.use('/login', _login.Login);
router.use('/tag', _tag.Tag);