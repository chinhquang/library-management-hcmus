import express from 'express'
import uuid from 'uuid/v4'

import { Authen, decodeToken, hashpwd } from '../util'
import { taiKhoan } from '../schema'

const TaiKhoan = express.Router()

TaiKhoan.get('/', Authen, async (req, res) => {
  let _listTaiKhoan = await taiKhoan.find({})
  res.json([..._listTaiKhoan])
})

TaiKhoan.get('/:ID', Authen, async (req, res) => {
  const { ID } = req.params
  let _taiKhoan = await taiKhoan.findOne({_id: ID})
  res.json(_taiKhoan)
})

TaiKhoan.post('/', async (req, res) => {
  let data = req.body

  const token = req.headers['authorization']
  if (token !== undefined) {
    const info = await decodeToken(req)
    if (info.code === 'admin') {
      data.code = 'mod'
    } else {
      data.code = 'member'
    }
  }
  else {
    data.code = 'member'
  }

  let logs = await taiKhoan.findOne({taiKhoan: data.taiKhoan})
  if (logs !== null) res.json({code: 0, msg: 'Tài khoản đã tồn tại'})
  else {
    data._id = uuid()
    data.matKhau = await hashpwd(data.matKhau)
    data.ngaySinh = +data.ngaySinh
    const _taiKhoan = new taiKhoan(data)
    _taiKhoan.save()
    res.json({code:1})
  }
})

TaiKhoan.put('/:ID', Authen, async (req, res) => {
  const { ID } = req.params
  let data = req.body

  let logs = await taiKhoan.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Tài khoản không tồn tại'})
  else {
    const _taiKhoan = await taiKhoan.findOneAndUpdate({_id: ID}, data, { new: true })
    res.json(_taiKhoan)
  }
})

TaiKhoan.delete('/:ID', Authen, async (req, res) => {
  const { ID } = req.params

  let logs = await taiKhoan.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Tài khoản không tồn tại'})
  else {
    await taiKhoan.findOneAndRemove({_id: ID})
    res.json({code:1})
  }
})

export { TaiKhoan }