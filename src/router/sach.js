import express from 'express'
import uuid from 'uuid/v4'

import { Authen, decodeToken } from '../util'
import { sach } from '../schema'

const Sach = express.Router()

Sach.get('/', async (req, res) => {
  let _listSach = await sach.find({}).populate('tag')
  res.json([..._listSach])
})

Sach.get('/:ID', async (req, res) => {
  const { ID } = req.params
  let _sach = await sach.findOne({_id: ID}).populate('tag')
  res.json(_sach)
})

Sach.post('/', Authen, async (req, res) => {
  let data = req.body

  const info = await decodeToken(req)
  if (info.code === 'mod' || info.code === 'admin') {
    let logs = await sach.findOne({seri: data.seri})
    if (logs !== null) res.json({code: 0, msg: 'Sách đã có trong kho'})
    else {
      data._id = uuid()
      data.ngayXuatBan = +data.ngayXuatBan
      data.trangThai = 1 // 1 = chưa mượn, 0 = đã mượn
      const _sach = new sach(data)
      _sach.save()
      res.json({code: 1})
    }
  }
  else {
    res.json({code:0, msg: 'Không có quyền thêm sách vào thư viện'})
  }
})

Sach.put('/:ID', Authen, async (req, res) => {
  const { ID } = req.params
  let data = req.body

  let logs = await sach.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Sách không có trong thư viện'})
  else {
    const _sach = await sach.findOneAndUpdate({_id: ID}, data, { new: true })
    res.json(_sach)
  }
})

Sach.delete('/:ID', Authen, async (req, res) => {
  const { ID } = req.params

  let logs = await sach.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Sách không có trong thư viện'})
  else {
    await sach.findOneAndDelete({_id: ID})
    res.json({code:1})
  }
})

export { Sach }