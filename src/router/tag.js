import express from 'express'
import uuid from 'uuid/v4'

import { Authen, decodeToken } from '../util'
import { tag } from '../schema'

const Tag = express.Router()

Tag.get('/', Authen, async (req, res) => {
  let _listTag = await tag.find({})
  res.json([..._listTag])
})

Tag.get('/:ID', Authen, async (req, res) => {
  const { ID } = req.params
  let _tag = await tag.findOne({_id: ID})
  res.json(_tag)
})

Tag.post('/', Authen, async (req, res) => {
  let data = req.body

  const info = await decodeToken(req.body)
  if (info.code === 'mod' || info.code ==' admin') {
    let logs = await tag.findOne({name: data.name})
    if (logs !== null) res.json({code: 0, msg: 'Nhãn đã có trong kho'})
    else {
      data._id = uuid()
      const _tag = new tag(data)
      _tag.save()
      res.json({code: 1})
    }
  } else {
    res.json({code:0, msg: 'Không có quyền thêm nhãn vào hệ thống'})
  }
})

Tag.put('/:ID', Authen, async (req, res) => {
  const { ID } = req.params
  let data = req.body

  let logs = await tag.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Nhãn không có trong hệ thống'})
  else {
    const _tag = await tag.findOneAndUpdate({_id: ID}, data, { new: true })
    res.json(_tag)
  }
})

Tag.delete('/:ID', Authen, async (req, res) => {
  const { ID } = req.params

  let logs = await tag.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Nhãn không có trong hệ thống'})
  else {
    await tag.findOneAndDelete({_id: ID})
    res.json({code:1})
  }
})

export { Tag }