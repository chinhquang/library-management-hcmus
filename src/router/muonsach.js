import express from 'express'
import uuid from 'uuid/v4'
import moment from 'moment'

import { Authen, decodeToken } from '../util'
import { muonSach, chiTiet, sach } from '../schema'

const MuonSach = express.Router()

MuonSach.get('/', Authen, async (req, res) => {
  let _listMuonSach = await muonSach.find({}).populate('taiKhoan').populate('chiTiet').populate('sach')
  res.json([..._listMuonSach])
})

MuonSach.get('/:ID', Authen, async (req, res) => {
  const { ID } = req.params
  let _muonSach = await muonSach.findOne({_id: ID}).populate('taiKhoan').populate('chiTiet').populate('sach')
  res.json(_muonSach)
})

MuonSach.get('/member', Authen, async (req, res) => {
  const info = await decodeToken(req)
  let _listMuonSach = await muonSach.find({_nguoiMuon: info._id}).populate('taiKhoan').populate('chiTiet').populate('sach')
  res.json([..._listMuonSach])
})

MuonSach.post('/', Authen, async (req, res) => {
  let data = req.body
  // {
  //   _nguoiMuon: String,
  //   _nguoiChoMuon: String,
  //   _chiTiet: {
  //     _sach: [String], // Tên sách
  //     ghiChu: String,
  //     ngayHenTra: Number
  //   }
  // }
  let { _nguoiMuon, _nguoiChoMuon, _chiTiet } = data
  let logs = _chiTiet._sach.map(v => {
    return sach.find({tenSach: v, trangThai: 1})
  })
  let dataLogs = await Promise.all(logs)
  const _sach = dataLogs.map(v => {
    return v[0]._id
  })
  __chiTiet = new chiTiet({
    _id: uuid(),
    _sach,
    ghiChu: _chiTiet.ghiChu,
    ngayMuon: moment().valueOf(),
    ngayHenTra: _chiTiet.ngayHenTra,
    trangThai: 0 // 0 = Người dùng yêu cầu mượn, 1 = Nhân viên đã cho mượn
  })
  let objChiTiet = null
  __chiTiet.save((err, obj) => objChiTiet = obj)
  __muonSach = new muonSach({
    _id: uuid(),
    _nguoiMuon, _nguoiChoMuon,
    _chiTiet: objChiTiet._id
  })
  __muonSach.save()
  res.json({code:1})
})

MuonSach.put('/:ID', Authen, async (req, res) => {
  // ID bảng mượn sách
  const { ID } = req.params
  // Dữ liệu chỉnh sửa bảng chi tiết
  let data = req.body

  let logs = await muonSach.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Quá trình mượn sách này không tồn tại'})
  else {
    await chiTiet.findOneAndUpdate({_id: data._id}, data, { new: true })
    const _muonSach = await muonSach.findOne({_id: ID}).populate('taiKhoan').populate('chiTiet').populate('sach')
    res.json(_muonSach)
  }
})

MuonSach.delete('/:ID', Authen, async (req, res) => {
  // ID bảng mượn sách
  const { ID } = req.params

  let logs = await muonSach.findOne({_id: ID})
  if (logs === null) res.json({code: 0, msg: 'Quá trình mượn sách này không tồn tại'})
  else {
    await chiTiet.findOneAndDelete({_id: logs._chiTiet})
    await muonSach.findOneAndDelete({_id: ID})
    res.json({code:1})
  }
})

export { MuonSach }