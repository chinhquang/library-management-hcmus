require('dotenv').config()
import express from 'express'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import { taiKhoan as model } from '../schema'
import { Authen, decodeToken } from '../util'

const Login = express.Router()

Login.post('/', async (req, res) => {
  let { taiKhoan, matKhau } = req.body
  let _taiKhoan = await model.findOne({taiKhoan})
  if (_taiKhoan === null) res.json({code:0, msg: 'Tài khoản không tồn tại'})
  else {
    const flag = bcrypt.compareSync(matKhau, _taiKhoan.matKhau)
    if (!flag) res.json({code:0, msg: 'Sai mật khẩu'})
    else {
      delete _taiKhoan.matKhau
      const token = jwt.sign({..._taiKhoan}, process.env.PRIVATE_KEY, { algorithm: 'HS512' })
      res.json({code:1, token})
    }
  }
})

Login.get('/', Authen, async (req, res) => {
  const info = await decodeToken(req)
  res.json(info)
})

export { Login }