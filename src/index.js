require('dotenv').config()
import '@babel/polyfill'

import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

import { router } from './router'

(async () => {
  const app = express()
  app.use(cors())
  app.use(bodyParser.json())
  const port = process.env.PORT || 12345

  await mongoose.connect(`${process.env.DB_URL}/${process.env.DB_NAME}`, {
    useNewUrlParser: true,
    dbName: 'lib'
  }).then(() => {
    console.log(`🚀 Database connected.`)
  }).catch((e) => {
    throw(e)
  })

  app.use('/api', router)

  app.listen(port, () => {
    console.log(`🚀 Server already running on http://localhost:${port}`)
  })
})()