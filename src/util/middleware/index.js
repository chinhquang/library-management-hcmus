require('dotenv').config()
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const key = process.env.PRIVATE_KEY

const Authen = (req, res, next) => {
  const token = req.headers['authorization']
  if (token === undefined) {
    res.json({code: 0, msg: 'Chưa đăng nhập'})
  } else {
    jwt.verify(token, key, { algorithm: 'HS512' }, err => {
      if (err) res.json({code: 0, msg: 'Token error'})
      else next()
    })
  }
}

const decodeToken = async ({headers: {authorization}}) => {
  const token = await jwt.verify(authorization, key, { algorithm: 'HS512' })
  return token['_doc']
}

const hashpwd = async (pwd) => {
  let hash = await bcrypt.hashSync(pwd, 10)
  return hash
}

export { Authen, decodeToken, hashpwd }