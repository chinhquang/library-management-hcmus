import mongoose from 'mongoose'

const tag_schema = new mongoose.Schema({
  _id: String,
  name: String
})

const tag = mongoose.model('tag', tag_schema)

export { tag }