import mongoose from 'mongoose'

const taiKhoan_schema = new mongoose.Schema({
  _id: String,
  hoTen: String,
  ngaySinh: Number,
  taiKhoan: String,
  matKhau: String,
  SDT: String,
  truongDH: String,
  mssv: String,
  code: String
})

const taiKhoan = mongoose.model('taiKhoan', taiKhoan_schema)

export { taiKhoan }