import mongoose from 'mongoose'

const chiTiet_schema = new mongoose.Schema({
  _id: String,
  _sach: [{ type: String, ref: 'sach' }],
  ghiChu: String,
  ngayMuon: Number,
  ngayHenTra: Number,
  ngayTra: Number,
  tongTien: Number,
  trangThai: Number
})

const chiTiet = mongoose.model('chiTiet', chiTiet_schema)

export { chiTiet }