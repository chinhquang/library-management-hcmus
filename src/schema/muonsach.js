import mongoose from 'mongoose'

const muonSach_schema = new mongoose.Schema({
  _id: String,
  _nguoiMuon: { type: String, ref: 'taiKhoan' },
  _nguoiChoMuon: { type: String, ref: 'taikhoan' },
  _chiTiet: { type: String, ref: 'chiTiet' }
})

const muonSach = mongoose.model('muonSach', muonSach_schema)

export { muonSach }