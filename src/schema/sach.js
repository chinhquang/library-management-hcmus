import mongoose from 'mongoose'

const sach_schema = new mongoose.Schema({
  _id: String,
  tenSach: String,
  tenTacGia: String,
  ngayXuatBan: Number,
  trangThai: Number,
  seri: String,
  tag: [{ type: String, ref: 'tag' }]
})

const sach = mongoose.model('sach', sach_schema)

export { sach }