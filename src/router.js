import express from 'express'
import { TaiKhoan } from './router/taikhoan'
import { Sach } from './router/sach'
import { Login } from './router/login'
import { Tag } from './router/tag'
import { MuonSach } from './router/muonsach'

const router = express.Router()
router.use('/taikhoan', TaiKhoan)
router.use('/sach', Sach)
router.use('/login', Login)
router.use('/tag', Tag)
router.use('muonsach', MuonSach)

export { router }